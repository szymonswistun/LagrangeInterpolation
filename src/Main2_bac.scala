/**
  * Created by hanter on 07.11.16.
  */
object Main2_bac{

  def parseT(l:Array[Float]): (Float,Float) = (l(0), l(1))

  def parse(s:List[String], l:List[(Float, Float)] = List()):List[(Float, Float)] = s match{
    case str :: r => parse(r, parseT(str.split(",").map(_.toFloat)) :: l)
    case _ => l.reverse
  }

  def input():List[(Float, Float)] = {
    parse(readLine().split(" ").toList)
  }

  /*def calcOmega(l:List[(Float, Float)], w:List[String] = Nil, id:Int = 1): List[String] = {
    if(id <= l.length) calcOmega(l, l.slice(0, id).map((n:(Float,Float)) =>
      if(n._1 < 0) s"(x + ${-n._1})"
          else if(n._1 > 0) s"(x - ${n._1})"
          else "x"
    ).mkString :: w, id + 1)
    else w.reverse
  }*/

  def genL(l:List[(Float,Float)], ind:Int):String = {
    l.indices.filter(_ != ind).map((i:Int) =>
      "(" + (
        if(l(i)._1 < 0) s"(x + ${-l(i)._1})"
        else if(l(i)._1 > 0) s"(x - ${l(i)._1})"
        else "x"
        ) + s" / ${l(ind)._1 - l(i)._1})"
    ).mkString
  }

  def mkForm(l:List[(Float, Float)]):String = {
    val ls = l.indices.map(genL(l, _))

    ( if(l(0)._2 > 0) l(0)._2 + ls(0)
    else ""
      ) + ((els:Array[String], ys:Array[Float]) => {
      els.indices.map((i:Int) => {
        if(ys(i) == 0) ""
        else if(ys(i) > 0) s" + ${ys(i)}${els(i)}"
        else s" - ${-ys(i)}${els(i)}"
      }).mkString
    })(ls.slice(1, ls.length - 1).toArray, l.indices.filter(_ > 0).map((i:Int) => l(i)._2).toArray)
  }

  def main(args: Array[String]) {
    val l = input()
    println(mkForm(l))
  }

}