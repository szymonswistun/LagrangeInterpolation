/**
  * Created by Szymon Świstun on 07.11.16.
  */
object Main{

  def parseT(l:Array[Float]): (Float,Float) = (l(0), l(1))

  def parse(strList:List[String], result:List[(Float, Float)] = List()):List[(Float, Float)] = strList match{
    case str :: rest => parse(rest, parseT(str.split(",").map(_.toFloat)) :: result)
    case _ => result.reverse
  }

  def functionGen(ind:Int, l:List[(Float, Float)]): (Float, Float) => Float = {
    (d: Float, _) => l.indices.filter(_ != ind).map((i:Int) => (d - l(i)._1) / (l(ind)._1 - l(i)._1)).product
  }

  def calcL(l:List[(Float, Float)]): Array[(Float, Float) => Float] = {
    l.indices.map((i:Int) => {
      functionGen(i, l)
    }).toArray
  }

  def calcPoly(a: Array[(Float, Float) => Float], l:List[(Float, Float)]): (Float, Float) => Float = (x:Float, y:Float) => {
    a.indices.map((i:Int) => l(i)._2 * a(i)(x, y)).sum
  }

  def input():List[(Float, Float)] = {
    parse(readLine().split(" ").toList)
  }

  def solve(x: Float, l:List[(Float, Float)]): Unit ={
    println(calcPoly(calcL(l), l)(x, 0))
  }

  def main(args: Array[String]) {
    val (l, x) = (input(), readLine().toFloat)
    solve(x, l)
  }

}